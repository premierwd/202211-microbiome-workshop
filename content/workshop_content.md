This website can be reached at: https://bit.ly/3V5ZVLl

## Microbiome Introduction
1. [Microbiome Introduction](content/lecture_slides/GCB_Microbiome_syk_final.pdf)

## Microbiome Bioinformatic Analysis
1. [Bioinformatic Analysis Overview](content/lecture_slides/microbiome_bioinformatics.pdf)
2. [Demultiplex](notebooks/demultiplex.Rmd)
3. [DADA2: From FASTQs to OTUs](notebooks/dada2_tutorial_1_6.Rmd)

## Microbiome Statistical Analysis
1. [Statistical Analysis Overview](content/lecture_slides/microbiome_statistical_analysis.pdf)
2. [Absolute Abundance Plots](notebooks/absolute_abundance_plots.Rmd)
3. [Alpha Diversity](notebooks/alpha_diversity.Rmd)
4. [Relative Abundance](notebooks/relative_abundance.Rmd)
5. [Beta Diversity & Ordination](notebooks/ordination.Rmd)

## Appendix
1. [Configuration File](notebooks/config.R)
2. [Download Atacama FASTQs](prep/atacama_download.Rmd)
3. [Download Taxonomic References](prep/download_dada_references.Rmd)
