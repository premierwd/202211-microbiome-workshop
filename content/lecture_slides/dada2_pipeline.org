#+TITLE:     Amplicon Bioinformatic Analysis: DADA2
#+AUTHOR:    Josh Granek
#+DATE:


* Bioinformatic Goals
** Bioinformatic Analysis					   :noexport:
*** Input: Raw FASTQ File(s)
   #+LATEX: \tiny
#+BEGIN_EXAMPLE
 M00698:36:000000000-AFBEL:1:1101:14738:1412 1:N:0:0
TTACGCTAACAGGCGGTAGCCTGGCAGGGTCAGGAAATCAATTAACTCATCGGAAGTGGTGATCTGTTCCATCAAGCGTGCGGCATCGTCA
+
ABBBABBBAFFFGGGGGGGGGGHGGHGGGCG2GF3FFGHHHHHHGGFGHEHHGGGEHHHHAGGHHGHHHFFDHFHHHGEGGGG@F@H?GHH
@M00698:36:000000000-AFBEL:1:1101:16483:1412 1:N:0:0
CTGCCAGTTGAACGACGGCGAGCAGTTATAAGCCAGCAGTTTGCCCGGATATTTCGCGTGGATAGCTTGTGCAAAGCGACGCGCCAGTTCC
+
AAABBFFFFFFFGGGGGGGGGGGGHHHHHHHHGHGHGHHHHHGHHHGGGGGHHHHGGGGGGGHHHGHHFFHHHHHGHGGGGGGGGGGHHHH
#+END_EXAMPLE
*** Output: Count Table
|            | Sample 1 | Sample 2 | …  | Sample N |
| /          |       <> |       <> | <> |       <> |
|------------+----------+----------+----+----------|
| Bacteria 1 |        0 |        0 |    |       64 |
|------------+----------+----------+----+----------|
| Bacteria 2 |       72 |        5 |    |        0 |
|------------+----------+----------+----+----------|
| …          |          |          |    |          |
|------------+----------+----------+----+----------|
| Bacteria N |        0 |       43 |    |        0 |
|------------+----------+----------+----+----------|

** Naive Approach: Assumptions
   - Library Prep is Perfect
   - Sequencing is Perfect
** Naive Approach: Counting
   #+ATTR_BEAMER: :overlay +(1)-
   1. Make an empty count table
   2. For each read in the FASTQ:
      1. If read sequence is already in count table, add 1 to that row
      2. Otherwise add a new row for the sequence and set its count to 1
** TODO Naive Approach: Counting Demo
*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
    | Sequence | Count |
    | /        | <     |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|
    |          |       |
    |          |       |
    |----------+-------|

*** A screenshot 					    :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
1. CAGCT
2. TATAA
3. TATGA
4. TGCGC
5. CGGGC
6. TGCGC
7. TGCGG
8. CAGCT
9. CGGGC
10. TGCGC

** Generate Toy Reads						   :noexport:
```{r}
makeseq = function(seqlen=5){
  paste(sample(c("A","T","C","G"), seqlen, replace=TRUE),collapse = "")
}

# sample(rep(makeseq(), 5), rep(makeseq(), 2), rep(makeseq(), 1))
uniq_seqs = c(makeseq(), makeseq(), makeseq(), makeseq())
for (i in seq(10)){
  cat(paste0(i, "."), sample(uniq_seqs, 
                     prob = c(0.5, 0.2, 0.3, 0.3),
                     1, replace=TRUE), fill = TRUE)
}

```

```{r}
makeseq(15)
```

** Naive Assumptions
   - +Library Prep is Perfect+
   - +Sequencing is Perfect+

*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
    | Sequence | Count |
    | /        |     < |
    |----------+-------|
    | CAGCT    |     2 |
    |----------+-------|
    | TATAA    |     2 |
    |----------+-------|
    | TGCGC    |     4 |
    |----------+-------|
    | CGGGC    |     2 |
    |----------+-------|
  
*** A screenshot 					    :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
1. CAGCT
2. TATAA
3. TATAA
4. TGCGC
5. CGGGC
6. TGCcC
7. TGCGC
8. CAGCT
9. CGGGa
10. TGCGC

** Tools for Bioinformatic Analysis
*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.3
    :END:
   - "Clustering"
     - Mothur
     - UCLUST
     - UPARSE
   - "Denoising"
     - DADA2
     - UNOISE3
     - Deblur
*** A screenshot 					    :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.7
    :END:
    #+ATTR_LaTeX: :width 0.7\textwidth :float t :placement [H]
    file:figures/dada2_accuracy.jpg [fn::[[https://benjjneb.github.io/dada2/index.html][DADA2 Website]]]


* Get Data (pre-DADA2) 						   :noexport:
** Get Data: Sources
   - Sequence Read Archive (SRA)
   - MG-RAST (Metagenomic Rapid Annotations using Subsystems Technology)
   - Sequencing Facility
** Get Data: Tools
   - curl
   - wget
   - ncftp
   - rsync
   - sftp
   - SRA Toolkit
** Get Data: Result
   - FASTQ(s) (gzip'ed)
     - Undetermined_S0_L001_I1_001.fastq.gz
     - Undetermined_S0_L001_R1_001.fastq.gz
     - Undetermined_S0_L001_R2_001.fastq.gz
   - Map File*
     - mydata_map.txt
   - Checksum*
     - md5sum.txt
* Validate Data (pre-DADA2)					   :noexport:
** Validate Data: Input
   - FASTQ(s) (gzip'ed)
     - Undetermined_S0_L001_I1_001.fastq.gz
     - Undetermined_S0_L001_R1_001.fastq.gz
     - Undetermined_S0_L001_R2_001.fastq.gz
   - Checksum*
     - md5sum.txt
   - Map File*
     - mydata_map.txt

** Validate Data: Output
# #+LATEX: \tiny
#+BEGIN_EXAMPLE
$ md5sum -c md5sum.txt
mydata_map.txt: OK
Undetermined_S0_L001_I1_001.fastq.gz: OK
Undetermined_S0_L001_R1_001.fastq.gz: OK
Undetermined_S0_L001_R2_001.fastq.gz: OK
#+END_EXAMPLE

** Validate Data: Tools
   - md5sum
* Assemble Metadata Table (pre-DADA2)
** Assemble Metadata Table: Why?
   Associate barcode with Sample
   - Label
   - Animal
   - Site
   - Phenotype
   - Treatment
   - Date
   - . . .
** Assemble Metadata Table: Input				   :noexport:
   - Existing Map
   - Publication
   - Notes
** Assemble Metadata Table: Output
   Metadata Table (Mapping File)
   #+LATEX: \tiny

| SampleID  | BarcodeSequence | Treatment |      DOB | Description             |
|-----------+-----------------+-----------+----------+-------------------------|
| PC.354    | AGCACGAGCCTA    | Control   | 20061218 | Control_mouse__I.D._354 |
| PC.355    | AACTCGTCGATG    | Control   | 20061218 | Control_mouse__I.D._355 |
| PC.356    | ACAGACCACTCA    | Control   | 20061126 | Control_mouse__I.D._356 |
| PC.481    | ACCAGCGACTAG    | Control   | 20070314 | Control_mouse__I.D._481 |
| PC.593    | AGCAGCACTTGT    | Control   | 20071210 | Control_mouse__I.D._593 |
| PC.607    | AACTGTGCGTAC    | Fast      | 20071112 | Fasting_mouse__I.D._607 |
| PC.634    | ACAGAGTCGGCT    | Fast      | 20080116 | Fasting_mouse__I.D._634 |
| PC.635    | ACCGCAGAGTCA    | Fast      | 20080116 | Fasting_mouse__I.D._635 |
| PC.636    | ACGGTGAGTGTC    | Fast      | 20080116 | Fasting_mouse__I.D._636 |


** Assemble Metadata Table: Tools				   :noexport:
   - Excel
   - Text Editor
   - Script
* Demultiplex (pre-DADA2)
** Demultiplex: Why?
   Split FASTQ File(s) by sample [fn:: Some data comes demultiplexed]
   # so reads for each sample are in their own FASTQ
** Demultiplex: Input
   - Sequence FASTQ(s)
     - Undetermined_S0_L001_I1_001.fastq.gz
     - Undetermined_S0_L001_R1_001.fastq.gz
   - Barcode FASTQ or Trimmed Versions [fn:: Some facilities incorporate barcodes in the sequence FASTQ, these will need to be extracted]
     - Undetermined_S0_L001_R2_001.fastq.gz 
   - Map File
     - mydata_map.txt 
** Demultiplex: Output
   Demultiplexed FASTQs
   - sampleA_R1.fastq.gz
   - sampleB_R1.fastq.gz
   - sampleC_R1.fastq.gz
   - . . .
   - sampleA_R2.fastq.gz
   - sampleB_R2.fastq.gz
   - sampleC_R2.fastq.gz
   - . . .
** Demultiplex: Tools						   :noexport:
   - split_libraries_fastq.py + split_sequence_file_on_sample_ids.py
   - fastq_multx
* Adapter Trimming (pre-DADA2)					   :noexport:
** Adapter Trimming: Why?
   Remove adapter contamination
   - Necessary for amplicons with large variation in length (e.g. ITS)
   - Generally unnecessary for 16S rRNA
*** TODO figure?						   :noexport:
** Adapter Trimming: Input
*** Adapter Sequence
    my_adapter.fasta
*** Demultiplexed FASTQs
   - sampleA_R1.fastq.gz
   - sampleB_R1.fastq.gz
   - sampleC_R1.fastq.gz
   - . . .
   - sampleA_R2.fastq.gz
   - sampleB_R2.fastq.gz
   - sampleC_R2.fastq.gz
   - . . .
** Adapter Trimming: Output 
   Trimmed FASTQs
   - sampleA_R1.trim.fastq.gz
   - sampleB_R1.trim.fastq.gz
   - sampleC_R1.trim.fastq.gz
   - . . .
   - sampleA_R2.trim.fastq.gz
   - sampleB_R2.trim.fastq.gz
   - sampleC_R2.trim.fastq.gz
   - . . .
*** Synchronized Trimming
    Depending on settings, some reads may be thrown out during trimming.  It is essential that if a read is thrown out, its paired read is thrown out too.  Most trimming software will do this for you if you input R1 and R2 files when you run.

** Adapter Trimming: Tools
   - fastq_mcf
   - Trimmomatic
   - cutadapt
   - seqtk
   - etc

* Filter and Trim
** Filter and Trim: Why?
   - Remove low quality parts of reads
   - Remove reads that are low quality overall
   - /Remove adapter contamination/
** R1 Read Quality
    #+ATTR_LaTeX: :height 0.7\textheight :float t :placement [H]
    file:figures/see-quality-F-1.png [fn::[[https://benjjneb.github.io/dada2/tutorial_1_6.html][DADA2 Tutorial]]]

    # green is the mean
    # orange is the median
    # dashed orange lines are the 25th and 75th quantiles.

** R2 Read Quality
    #+ATTR_LaTeX: :height 0.7\textheight :float t :placement [H]
    file:figures/see-quality-R-1.png [fn::[[https://benjjneb.github.io/dada2/tutorial_1_6.html][DADA2 Tutorial]]]

** Filter and Trim: Input					   :noexport:
   Trimmed FASTQs (or Demultiplexed)
   - sampleA_R1.trim.fastq.gz
   - sampleB_R1.trim.fastq.gz
   - sampleC_R1.trim.fastq.gz
   - . . .
   - sampleA_R2.trim.fastq.gz
   - sampleB_R2.trim.fastq.gz
   - sampleC_R2.trim.fastq.gz
   - . . .
** Filter and Trim: Output					   :noexport:
   Trimmed and filtered FASTQs
** Filter and Trim: Tools					   :noexport:
   dada2::filterAndTrim()
** Filter and Trim: Parameters					   :noexport:
   #+ATTR_BEAMER: :overlay +(1)-
   - truncQ: Truncate reads at the first instance of a quality score less than or equal to truncQ.
   - truncLen: Truncate reads after truncLen bases. *Don't use for ITS*
   - trimLeft: The number of nucleotides to remove from the start of each read.
   - minQ: After truncation, reads contain a quality score less than minQ will be discarded.
   - maxEE: After truncation, reads with higher than maxEE "expected errors" will be discarded. ~EE = sum(10^(-Q/10))~
   - rm.phix: Discard reads that match against the phiX genome
** Filter and Trim: Notes					   :noexport:
   Paired-End Reads need to be run simultaneously to keep them in sync
* Learn Error Rates
** Learn Error Rates: Why?
   Build an error model from data
   | Phred | A:A | A:T | A:C | A:G | C:A | ... | G:G |
   |     / | <>  | <>  | <>  | <>  | <>  | <>  | <>  |
   |-------+-----+-----+-----+-----+-----+-----+-----|
   |     1 | ?   | ?   | ?   | ?   | ?   | ... | ?   |
   |     2 | ?   | ?   | ?   | ?   | ?   | ... | ?   |
   |     3 | ?   | ?   | ?   | ?   | ?   | ... | ?   |
   |   ... | ... | ... | ... | ... | ... | ... | ... |
   |    40 | ?   | ?   | ?   | ?   | ?   | ... | ?   |

** Learn Error Rates: Input 
   Filtered and Trimmed FASTQs
** Learn Error Rates: Output
   error model

   | Phred |     A:A |     A:T |     A:C |     A:G | ... |     G:G |
   |     / |      <> |      <> |      <> |      <> | <>  |      <> |
   |-------+---------+---------+---------+---------+-----+---------|
   |     1 | 0.27042 | 0.23546 | 0.24245 | 0.25167 | ... | 0.24492 |
   |     2 | 0.27927 | 0.23248 | 0.24764 | 0.24062 | ... | 0.25699 |
   |     3 | 0.26260 | 0.25353 | 0.24638 | 0.23749 | ... | 0.23728 |
   |   ... |     ... |     ... |     ... |     ... | ... |     ... |
   |    40 | 0.99894 | 0.00022 | 0.00022 | 0.00062 | ... | 0.99724 |
** Learn Error Rates: Tools					   :noexport:
   dada2::learnErrors()
** Learn Error Rates: Notes					   :noexport:
   Separate error models need to be built for R1 and R2
* Dereplication
** Dereplication
   Summarize reads into unique observed reads, with quality summary and count

*** A screenshot 					    :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:

| Sequence | Quality |
| /        |      <> |
|----------+---------|
| CAGCT    |   99989 |
| TATAA    |   99998 |
| TATAA    |   98999 |
| TGCGC    |   99988 |
| CGGGC    |   98989 |
| TGCcC    |   99949 |
| TGCGC    |   99989 |
| CAGCT    |   99999 |
| CGGGa    |   98983 |
| TGCGC    |   99989 |


*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
    | Sequence | Count | Quality |
    | /        |    <> |      <> |
    |----------+-------+---------|
    | CAGCT    |     2 |   99989 |
    |----------+-------+---------|
    | TATAA    |     2 |   99998 |
    |----------+-------+---------|
    | TGCGC    |     3 |   99988 |
    |----------+-------+---------|
    | CGGGC    |     1 |   99999 |
    |----------+-------+---------|
    | TGCcC    |     1 |   99948 |
    |----------+-------+---------|
    | CGGGa    |     1 |   99993 |
    |----------+-------+---------|

** Dereplication: Input						   :noexport:
   Filtered and Trimmed FASTQs
** Dereplication: Output						   :noexport:
   Unique reads with summarized quality and counts
** Dereplication: Tools						   :noexport:
   dada2::derepFastq()
** Dereplication: Notes						   :noexport:
   Dereplication is done separately for R1 and R2
* Sample Inference
** Sample Inference: Why?
   Attempt to determine the true sequences from which reads were derived

*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
    | Sequence | Count | Quality |
    | /        |    <> |      <> |
    |----------+-------+---------|
    | CAGCT    |     2 |   99989 |
    |----------+-------+---------|
    | TATAA    |     2 |   99998 |
    |----------+-------+---------|
    | TGCGC    |     3 |   99988 |
    |----------+-------+---------|
    | CGGGC    |     1 |   99999 |
    |----------+-------+---------|
    | TGCcC    |     1 |   99948 |
    |----------+-------+---------|
    | CGGGa    |     1 |   99993 |
    |----------+-------+---------|

*** A text section 						      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:

** Sample Inference: Input 
   - Dereplicated Reads
   - Error Model
    | Sequence | Count | Quality |
    | /        |    <> |      <> |
    |----------+-------+---------|
    | CAGCT    |     2 |   99989 |
    |----------+-------+---------|
    | TATAA    |     2 |   99998 |
    |----------+-------+---------|
    | TGCGC    |     3 |   99988 |
    |----------+-------+---------|
    | CGGGC    |     1 |   99999 |
    |----------+-------+---------|
    | TGCcC    |     1 |   99948 |
    |----------+-------+---------|
    | CGGGa    |     1 |   99993 |
    |----------+-------+---------|

   | Phred |     A:A |     A:T |     A:C |     A:G | ... |     G:G |
   |     / |      <> |      <> |      <> |      <> | <>  |      <> |
   |-------+---------+---------+---------+---------+-----+---------|
   |     1 | 0.27042 | 0.23546 | 0.24245 | 0.25167 | ... | 0.24492 |
   |     2 | 0.27927 | 0.23248 | 0.24764 | 0.24062 | ... | 0.25699 |
   |     3 | 0.26260 | 0.25353 | 0.24638 | 0.23749 | ... | 0.23728 |
   |   ... |     ... |     ... |     ... |     ... | ... |     ... |
   |    40 | 0.99894 | 0.00022 | 0.00022 | 0.00062 | ... | 0.99724 |


** Sample Inference: Output
   Inferred read sequences with counts
    | Sequence | Count |
    | /        |    <> |
    |----------+-------|
    | CAGCT    |     2 |
    |----------+-------|
    | TATAA    |     2 |
    |----------+-------|
    | TGCGC    |     4 |
    |----------+-------|
    | CGGGC    |     2 |
    |----------+-------|
** Sample Inference: Tools					   :noexport:
   dada2::dada()
** Sample Inference: Notes					   :noexport:
   Sample Inference is done separately for R1 and R2

* Merge Paired Reads
** Merge Paired Reads: Why?
   Collapse read pairs into a single sequence for each inferred amplicon

#+BEGIN_EXAMPLE
R1:       ATACCCTAGTGC
R2:          CCCTAGTGCCGT

Merged:   ATACCCTAGTGCCGT
#+END_EXAMPLE 


** Merge Paired Reads: Input 
   - R1 
     - Inferred Sequences 
     - Dereplicated Sequences 
   - R2 
     - Inferred Sequences 
     - Dereplicated Sequences 
** Merge Paired Reads: Output
Inferred amplicon sequences

** Merge Paired Reads: Tools					   :noexport:
   dada2::mergePairs()

* Construct Sequence Table
** Construct Sequence Table
Generate count table

|       | Sample 1 | Sample 2 | Sample N |
| /     |       <> |       <> |       <> |
|-------+----------+----------+----------|
| CAGCT |        2 |        0 |       64 |
|-------+----------+----------+----------|
| TATAA |        2 |        5 |        0 |
|-------+----------+----------+----------|
| TGCGC |        4 |        0 |        7 |
|-------+----------+----------+----------|
| CGGGC |        2 |       43 |        0 |
|-------+----------+----------+----------|


** Construct Sequence Table: Input					   :noexport:
   Merged sequences
** Construct Sequence Table: Output					   :noexport:
   Count table
** Construct Sequence Table: Tools					   :noexport:
   dada2::makeSequenceTable()

* Remove Chimeras
** Remove Chimeras: Why?
   Library preparation is imperfect, so it generates chimeric amplicons
    #+ATTR_LaTeX: :height 0.6\textheight :float t :placement [H]
    file:figures/chimera_fig.png [fn::[[https://doi.org/10.1186/s12864-019-5847-2][Omelina, E.S., et al. BMC Genomics 20, 536 (2019)]]]


** Remove Chimeras: Input 					   :noexport:
   Count Table
** Remove Chimeras: Output					   :noexport:
   Count Table *without* chimeras
** Remove Chimeras: Tools					   :noexport:
   dada2::removeBimeraDenovo()

* Assign Taxonomy
** Assign Taxonomy: Why?
   Relate sequences in our count table to specific bacteria
   
|       | Sample 1 | Sample 2 | Sample N |
| /     |       <> |       <> |       <> |
|-------+----------+----------+----------|
| CAGCT |        2 |        0 |       64 |
|-------+----------+----------+----------|
| TATAA |        2 |        5 |        0 |
|-------+----------+----------+----------|
| TGCGC |        4 |        0 |        7 |
|-------+----------+----------+----------|
| CGGGC |        2 |       43 |        0 |
|-------+----------+----------+----------|
** Assign Taxonomy: Input 					   :noexport:
   Chimera-free merged sequences
** Assign Taxonomy: Output
   Mapping from sequences to specific bacteria
   #+LATEX: \tiny

|             | Kingdom  | Phylum           | Class               | Order                 | Family             | Genus             |
| /           | <        |                  |                     |                       |                    |                   |
|-------------+----------+------------------+---------------------+-----------------------+--------------------+-------------------|
| CAGCT       | Bacteria | Actinobacteria   | Rubrobacteria       | Rubrobacterales       | Rubrobacteriaceae  | Rubrobacter       |
| TATAA       | Bacteria | Gemmatimonadetes | Gemmatimonadetes    | Gemmatimonadales      | Gemmatimonadaceae  | NA                |
| TGCGC       | Bacteria | Actinobacteria   | Actinobacteria      | Pseudonocardiales     | Pseudonocardiaceae | Crossiella        |
| CGGGC       | Bacteria | Proteobacteria   | Alphaproteobacteria | Sphingomonadales      | Sphingomonadaceae  | Sphingomonas      |
| ...         |          |                  |                     |                       |                    |                   |
| Bacteria 5  | Bacteria | Firmicutes       | Bacilli             | Bacillales            | Paenibacillaceae   | Ammoniphilus      |
| Bacteria 6  | Bacteria | Actinobacteria   | Thermoleophilia     | Gaiellales            | Gaiellaceae        | Gaiella           |
| Bacteria 7  | Bacteria | Actinobacteria   | Actinobacteria      | Micrococcales         | Micrococcaceae     | Pseudarthrobacter |
| Bacteria 8  | Bacteria | Proteobacteria   | Gammaproteobacteria | Betaproteobacteriales | Burkholderiaceae   | Ralstonia         |
| Bacteria 9  | Bacteria | Actinobacteria   | Actinobacteria      | Corynebacteriales     | Mycobacteriaceae   | Mycobacterium     |
| Bacteria 10 | Bacteria | Proteobacteria   | Alphaproteobacteria | Rhizobiales           | Xanthobacteraceae  | Bradyrhizobium    |
| Bacteria 11 | Bacteria | Actinobacteria   | Nitriliruptoria     | Nitriliruptorales     | Nitriliruptoraceae | NA                |
| Bacteria N  | Bacteria | Proteobacteria   | Gammaproteobacteria | Nitrosococcales       | Nitrosococcaceae   | wb1-P19           |




** Assign Taxonomy: Tools					   :noexport:
   dada2::assignTaxonomy()



* Generate Phyloseq Object					   :noexport:
** Generate Phyloseq Object: Why?
   Phyloseq objects organize multiple aspects of our results and ease downstream analysis and visualization
** Generate Phyloseq Object: Input 
   - Count Table
   - Metadata Table
   - Taxonomic Assignment
   - Phylogenetic Tree (optional)
** Generate Phyloseq Object: Output
   Phyloseq Object
** Generate Phyloseq Object: Tools
   phyloseq::phyloseq()

* Save Phyloseq as RDS					   :noexport:
** Save Phyloseq as RDS: Why?
   - Generating the final phyloseq object from raw FASTQs is time consuming, we would prefer to not repeat it everytime we want to play with the results
   - The Phyloseq object is a very space efficient representation of the processed data
** Save Phyloseq as RDS: Input
   - Phyloseq object
   - Name for RDS file
** Save Phyloseq as RDS: Output
    RDS file
** Save Phyloseq as RDS: Tools
   readr::write_rds()


* Beamer configuration ||||||   :noexport:
  :CONFIGURATION: 
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  en
#+OPTIONS:   num:t toc:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS:   tex:t d:nil todo:nil pri:nil tags:nil
#+OPTIONS:   timestamp:t

# this allows defining headlines to be exported/not be exported
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport

** Basic
# this triggers loading the beamer menu (C-c C-b) when the file is read
#+startup: beamer

#+LaTeX_CLASS: beamer

#    LATEX CLASS OPTIONS
# [bigger]
# [presentation]
# [handout] : print handouts, i.e. slides with overlays will be printed with
#   all overlays turned on (no animations).
# [notes=show] : show notes in the generated output (note pages follow the real page)
# [notes=only] : only render the nodes pages

# this setting affects whether the initial PSI picture correctly fills
# the title page, since it scales the title text. One can also use the
# notes=show or notes=only options to produce notes pages in the output.
# #+LaTeX_CLASS_OPTIONS: [t,10pt,notes=show]

#+LaTeX_CLASS_OPTIONS: [t,12pt]


#+COLUMNS: %20ITEM %13BEAMER_env(Env) %6BEAMER_envargs(Args) %4BEAMER_col(Col) %7BEAMER_extra(Extra)

# export second level headings as beamer frames. All headlines below
# the org-beamer-frame-level (i.e. below H value in OPTIONS), are
# exported as blocks
#+OPTIONS: H:2

** Beamer Theme Definition
# https://hartwork.org/beamer-theme-matrix/
# http://www.deic.uab.es/%7Eiblanes/beamer_gallery/index_by_theme_and_color.html 

# ------------------------
#+BEAMER_THEME: default 
#+BEAMER_COLOR_THEME: rose 
# ------------------------

# Note: custom style files can be placed centrally in the user specific directory
# ~/texmf/tex. This will be searched recursively, so substructures are possible.
# q.v. http://tex.stackexchange.com/questions/1137/where-do-i-place-my-own-sty-or-cls-files-to-make-them-available-to-all-my-te

# One could also fine tune a number of theme settings instead of specifying the full theme
# #+BEAMER_COLOR_THEME: default
# #+BEAMER_FONT_THEME:
# #+BEAMER_INNER_THEME:
# #+BEAMER_OUTER_THEME: miniframes [subsection=false]
# #+LATEX_CLASS: beamer


# Get rid of navigation bullets at top?
#+BEAMER_HEADER: \beamertemplatenavigationsymbolsempty


** changes to BeginSection for TOC and navigation
#+BEAMER_HEADER: \AtBeginSection[]{

# This line inserts a table of contents with the current section highlighted at
# the beginning of each section
#+BEAMER_HEADER: \begin{frame}<beamer>\frametitle{Topic}\tableofcontents[currentsection]\end{frame}

# In order to have the miniframes/smoothbars navigation bullets even though we do not use subsections 
# q.v. https://tex.stackexchange.com/questions/2072/beamer-navigation-circles-without-subsections/2078#2078
#+BEAMER_HEADER: \subsection{}
#+BEAMER_HEADER: }

** misc configuration
# I want to define a style for hyperlinks
#+BEAMER_HEADER: \hypersetup{colorlinks=true, linkcolor=blue}

# this can be used to define the transparency of the covered layers
#+BEAMER: \setbeamercovered{transparent=30}
# #+LaTeX_HEADER: \def\thefootnote{\xdef\@thefnmark{}\@footnotetext}

# Set footnote mark to "white" so it is not visible (i.e. blends in with background)
#+BEAMER_HEADER: \setbeamercolor{footnote mark}{fg=white}

# Get rid of navigation symbols in bottom right of slide
#+BEAMER_HEADER: \beamertemplatenavigationsymbolsempty

# Trying to move footnotes
# #+BEAMER_HEADER: \setbeamertemplate{footnote}{%
# #+BEAMER_HEADER:   \parindent 0em\noindent%
# #+BEAMER_HEADER:   \raggedright
# #+BEAMER_HEADER:   \usebeamercolor{footnote}\hbox to 0.8em{\hfil\insertfootnotemark}\insertfootnotetext\par%
# #+BEAMER_HEADER: }

# Smaller footnotes
#+BEAMER_HEADER: \setbeamerfont{footnote}{size=\tiny}

** Some remarks on options
   - [[info:org#Export%20settings][info:org#Export settings]]
   - The H:2 setting in the options line is important for setting the
     Beamer frame level. Headlines will become frames when their level
     is equal to =org-beamer-frame-level=.
   - ^:{} interpret abc_{subs} as subscript, but not abc_subs
   - num:t configures whether to use section numbers. If set to a number
     only headlines of this level or above will be numbered
   - ::t defines that lines starting with ":" will use fixed width font
   - |:t include tables in export
   - -:t Non-nil means interpret "\-", "--" and "---" for export.
   - f:t include footnotes
   - *:t Non-nil means interpret
     : *word*, /word/, _word_ and +word+.
   - <:t toggle inclusion of timestamps
   - timestamp:t include a document creation timestamp into the exported file
   - todo:t include exporting of todo keywords
   - d:nil do not export org heading drawers
   - tags:nil do not export headline tags

** addtional LaTeX packages

   # for generating example texts for testing
   #+BEAMER_HEADER: \usepackage{blindtext}
:END:

