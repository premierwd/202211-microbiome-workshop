#!/usr/bin/env bash
set -u


if [ $# -ne 1 ]; then
    echo ""
    echo "--------------------------------------------------------"
    echo "This script generates a beamer PDF from an org-mode file"
    echo "You need to provide the name of the org-mode file as a command line argument!!"
    echo "--------------------------------------------------------"
    echo ""
    exit 1
fi

ORG_FILENAME=$1
BASE_FILENAME=${ORG_FILENAME%".org"}
LATEX_FILENAME="${BASE_FILENAME}.tex"
PDF_FILENAME="${BASE_FILENAME}.pdf"

echo $ORG_FILENAME
echo $BASE_FILENAME
echo $LATEX_FILENAME

DOCKER_IMAGE="texlive/texlive:latest-full"
# DOCKER_IMAGE="texlive/texlive:latest-medium"
#----------------------------

# /Applications/Aquamacs.app/Contents/MacOS/bin/emacs $1 --batch -f org-latex-export-to-pdf --kill
# emacs $1 --batch -f org-latex-export-to-pdf --kill

emacs $ORG_FILENAME --batch -f org-beamer-export-to-latex --kill

#----------------------------
for num in {1..3}
do
  docker run --rm -t -i -v "$PWD":"$PWD" -w "$PWD" $DOCKER_IMAGE pdflatex -interaction=batchmode $LATEX_FILENAME
done

#----------------------------
open $PDF_FILENAME
