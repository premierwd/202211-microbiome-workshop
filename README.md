This website can be reached at: https://bit.ly/3V5ZVLl

[Workshop Content](content/workshop_content.md)

# Computing
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
    - [Getting started with DCC OnDemand](misc/ondemand_howto.md)
- [Initial download of workshop content](misc/git_cloning.md)
- [Update workshop content](misc/git_pull.md)

# After the Workshop
- [Other Workshops](misc/other_workshops.md)
- [Computing after the Workshop](misc/post_course_resources.md)
