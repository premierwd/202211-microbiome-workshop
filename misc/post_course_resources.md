-   [High Priority](#high-priority)
    -   [Important Notes](#important-notes)
-   [Maintaining Personal Files](#maintaining-personal-files)
    -   [Tar and Download](#tar-and-download)
        -   [Tarring](#tarring)
        -   [Downloadng the tarball](#downloadng-the-tarball)
        -   [Unpacking the tarball](#unpacking-the-tarball)
-   [Accessing Course Material](#accessing-course-material)
-   [Computing Environment](#computing-environment)
    -   [Using the Computing Environment After the
        Workshop](#using-the-computing-environment-after-the-workshop)
        -   [Duke Affiliates](#duke-affiliates)
        -   [People from outside Duke](#people-from-outside-duke)
    -   [Workshop Singularity Image and
        Recipe](#workshop-singularity-image-and-recipe)

``` r
library(here)
```

    ## here() starts at /hpc/home/josh/project_repos/teaching/202211-microbiome-workshop

``` r
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

<!-- #region -->

# High Priority

If you have **created or modified files** on DCC that you would like to
preserve, we recommend that you follow the instructions for [Maintaining
Personal Files](#Maintaining-Personal-Files). - We recommend you do this
before *as soon as possible*, if you are not a Duke affiliate, you may
lose access to your account shortly after the workshop. - Keep in mind
that you only need to worry about this for files that you have **created
or modified**. The course material that we created and shared with you
will continue to be publicly available.

## Important Notes

See details below, but please keep the following in mind: 1. The course
material will remain in the [Microbiome Workshop Gitlab
Repository](https://gitlab.oit.duke.edu/premierwd/202211-microbiome-workshop)
and will be publicly available, in perpetuity (or as long as
<https://gitlab.oit.duke.edu/> continues to exist), regardless of your
affiliation with Duke (or lack thereof). See below for details. 2. The
configuration and build information for the workshop Singularity
container will remain in the [Workshop Singularity
Image](https://gitlab.oit.duke.edu/hiv_r25/rstudio-singularity-2021-2022-hiv)
and will be publicly available, in perpetuity (or as long as
<https://gitlab.oit.duke.edu/> continues to exist), regardless of your
affiliation with Duke (or lack thereof). 3. The Workshop Singularity
Image will remain in the Duke Singularity Registry and will be publicly
available, in perpetuity (as long as the Duke Singularity Registry
continues to exist and freely host images), regardless of your
affiliation with Duke (or lack thereof). See [Workshop Singularity Image
and Recipe](#Workshop-Singularity-Image-and-Recipe) below for details

# Maintaining Personal Files

## Tar and Download

### Tarring

#### Tarring: Only Notebooks

If you only want to get *Rmarkdown notebooks*, you could use the
following to only grab the notebooks from 2022-mic. This will skip a lot
of stuff you probably don’t want. This archive file will be in your home
directory and will be named 2022-mic-notebooks.tar.gz
<!-- #endregion -->

``` r
tarfile_path=path.expand("~/2022-microbiome-notebooks.tar.gz")
here() %>%
  setwd

here() %>%
  list.files(recursive = TRUE, 
             pattern=".Rmd",
             full.names = FALSE) %>%
  tar(tarfile=tarfile_path,
      files=.,
      compression = "gzip")
```

    ## Warning in sprintf(gettext(fmt, domain = domain), ...): one argument not used by
    ## format 'invalid gid value replaced by that for user 'nobody''

    ## Warning: invalid gid value replaced by that for user 'nobody'

#### Tarring: Only Notebooks with a common name

If you want only modified notebooks **and** you saved them with a
standard naming scheme, e.g. leaving `-Copy1` in the name, for example,
renaming `demultiplex.Rmd` to `demultiplex-Copy1.Rmd`, you could use the
following to only grab the modified files

``` r
tarfile_path=path.expand("~/2022-microbiome-copyof.tar.gz")
here() %>%
  setwd

here() %>%
  list.files(recursive = TRUE, 
             pattern="CopyOf",
             full.names = FALSE) %>%
  tar(tarfile=tarfile_path,
      files=.,
      compression = "gzip")
```

<!-- #region -->

### Downloadng the tarball

Now you can do one of the following to download the tarball to your
laptop.

1.  In the RStudio *Files* pane, click on *Home* to navigate to the
    directory where you saved the tarball. T
2.  Click the checkbox next to `2022-microbiome-notebooks.tar.gz`
3.  In the *More* menu of the *Files* pane, select *Export*, then click
    the *Download* in the dialog box that pops up.

### Unpacking the tarball

On a Mac you can “untar” by double clicking on the file in finder, or at
the terminal with the command
`tar -zxf 2022-microbiome-notebooks.tar.gz`.

On Windows, you can download software that will do it, such as
[7-Zip](http://www.7-zip.org/)

> If you named your tarball differently, you should substitute whatever
> name you used above.

# Accessing Course Material

You can access the course material in three different ways:

1.  You can browse and download the material from the [Microbiome
    Workshop Gitlab
    Repository](https://gitlab.oit.duke.edu/premierwd/202211-microbiome-workshop)
    by clicking on the Download button, which is right next to the blue
    **Clone** button. It will give you a choice of format you want. The
    best options are probably “zip” or “tar.gz”
2.  You can **clone** the repo using git:
    `git clone https://gitlab.oit.duke.edu/premierwd/202211-microbiome-workshop.git`

# Computing Environment

## Using the Computing Environment After the Workshop

### Duke Affiliates

If you are a Duke Affiliate, you can continue to use DCC OnDemand.

1.  If you did not have a DCC account before the course, you will need
    to [follow these
    instructions](https://oit-rc.pages.oit.duke.edu/rcsupportdocs/dcc/#getting-a-dcc-account)
    to request one.
2.  When launching a container, you will need to change the *Account*
    and *Partition* values to an account and partition to which you have
    access.

### People from outside Duke

#### Open OnDemand

The computing environment for our course ran within an [Open
OnDemand](https://openondemand.org/) system. Look at your own
institution to find out if you have access to a cluster running [Open
OnDemand](https://openondemand.org/). If so, you can ask the
administrators if you can run the MIC Course environment there.

1.  You will need the [Singularity Recipe or Singularity
    Image](#Workshop-Singularity-Image-and-Recipe)
2.  You may need our [Open OnDemand Batch Connect
    App](https://gitlab.oit.duke.edu/chsi-informatics/ood_apps/microbiome-workshop-bc)

If [Open OnDemand](https://openondemand.org/) is not installed on your
local cluster, ask the system administrators to install it. It is free,
was developed with funding from the US National Science Foundation, and
is becoming a standard!

You can also run the Singularity image on a cluster or a local server
without Open OnDemand, but that is more complicated!

## Workshop Singularity Image and Recipe

The computing environment that we used in the MIC course ran withing a
Singularity Container. The Singularity Image Recipe is here: [Workshop
Singularity
Image](https://gitlab.oit.duke.edu/hiv_r25/rstudio-singularity-2021-2022-hiv)
and the built image can be pulled (downloaded) to your current directory
by running the following command in a terminal on a computer where
singularity is installed

You can pull (download) the singularity image to your current directory
by running the following command in a terminal on a computer where
singularity is installed

    singularity pull --dir . oras://gitlab-registry.oit.duke.edu/hiv_r25/rstudio-singularity-2021-2022-hiv:v038

> This command will not work within the workshop environment
> (Singularity is not installed there)

The [SingularityCE User
Guide](https://docs.sylabs.io/guides/latest/user-guide/) has information
on installing and using Singularity
